if not StrategosBattleground_Localizations then
    StrategosBattleground_Localizations = {}
end
if not StrategosBattleground_ServerLocalizations then
    StrategosBattleground_ServerLocalizations = {}
end

StrategosBattleground_Localizations.itIT = {
    WARSONG_FLAG_GROUND = "A TERRA",
    ARATHI_BOARD_STALL = "Stallo",
    ARATHI_BOARD_WINNING_FACTION0 = "L'Orda vincerà in: %time",
    ARATHI_BOARD_WINNING_FACTION1 = "L'Alleanza vincerà in: %time",
    ARATHI_BOARD_WIN_FACTION0 = "Vittoria dell'Orda!",
    ARATHI_BOARD_WIN_FACTION1 = "Vittoria dell'Alleanza!",
    WARSONG_LOWHEALTH_CHAT_WARN0 = "Il \\124cffff0000\\124Hplayer:%pname\\124h[portabandiera dell'Orda]\\124h\\124r è sotto il %health% di salute!",
    WARSONG_LOWHEALTH_CHAT_WARN1 = "Il \\124cff00b0ff\\124Hplayer:%pname\\124h[portabandiera dell'Alleanza]\\124h\\124r è sotto il %health% di salute!",
    WARSONG_LOWHEALTH_CHAT_WARN0_ALT = "Il portabandiera dell'ORDA è sotto il %health% di salute!",
    WARSONG_LOWHEALTH_CHAT_WARN1_ALT = "Il portabandiera dell'ALLEANZA è sotto il %health% di salute!",
    BG_START_IN_CHAT = "Inizia in: %time",
    BG_START_IN_UI = "[BG] Inizia in",
    SETTINGS_TITLE = "Impostazioni di Strategos Battleground",
    SETTINGS_SERVER_LANGUAGE = "Lingua del server",
    SETTINGS_GENERAL_SECTION = "Generali",
    SETTINGS_AUTO_JOIN = "Accodamento automatico",
    SETTINGS_AUTO_JOIN_GROUP = "Accodamento automatico come gruppo",
    SETTINGS_WSG_SECTION = "Forra dei Cantaguerra",
    SETTINGS_WSG_CARRIERS_SIDE = "Lato dei portabandiera:",
    SETTINGS_WSG_CARRIERS_SIDE_LEFT = "Sinistro",
    SETTINGS_WSG_CARRIERS_SIDE_RIGHT = "Destro",
    SETTINGS_WSG_CARRIERS_HEALTH_STYLE = "Style della vita dei portabandiera:",
    SETTINGS_WSG_CARRIERS_HEALTH_STYLE_COLOR = "Colore",
    SETTINGS_WSG_CARRIERS_HEALTH_STYLE_BAR = "Barra",
    SETTINGS_WSG_CARRIERS_HEALTH_WARNS = "Allerta chat vita bassa dei portabandiera:",
    SETTINGS_WSG_CARRIERS_HEALTH_WARNS_ENABLED = "Attivata",
    SETTINGS_WSG_CARRIERS_HEALTH_WARNS_DISABLED = "Disattivata",
    SETTINGS_WSG_CARRIERS_HEALTH_WARNS_NO_LINK = "Con player link (testo colorato)"
}
