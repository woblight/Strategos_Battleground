# Strategos_Battleground
Addon for WoW 1.12 (vanilla)
Enhance battleground experience, allows faster decisions about your battleground's objectives.

## Dependencies:
- StrategosCore
- EmeraldFramework
- Strategos_Minimap (optional, raccomanded)

## Slash Commands
/sbg /strbg /strategosbg

## Features:

- Auto-join battlegrounds queues when talking to battlemasters
- Battleground start timer

### Warsong Gulch
- Frame with carriers health, class and name (click to target)
- Low Heath warnings for carriers
- Flag respawn timer
- Flag reset timer
- Strategos clients are able to communicate each other:
  - Flag carriers names on join
  - Enemy flag carrier's health when out of range
  - Enemy flag carrier's engagements

### Arathi Basin
- Time to win frame

## Additional minimap features with Battleground_Minimap:

### Warsong Gulch
- Friendly flag carrier's highligh
- Ping when enemies' flag carrier is engaged (by combat log, raccomanded increasing combat log range)
  - small -> melee hit
  - large -> ranged/spell

### Arathi Basin/Alterac Valley
- Nodes timers
